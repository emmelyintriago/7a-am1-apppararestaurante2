package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class PedidoActivity extends AppCompatActivity {

    ImageView iniciar = null;
    Button iniciar2 = null;
    Button iniciar3 = null;
    ImageView iniciar4 = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);

        //Clic en regresar para ir al menú
        iniciar = (ImageView) findViewById(R.id.btnRegresarP);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar);
            }
        });

        //clic en finalizar pedido para ir a pantalla de agradecimiento
        iniciar2 = (Button) findViewById(R.id.btnFinlizar);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), FinalActivity.class);
                startActivity(cambiar2);
            }
        });


        //clic en cancelar pedido para ir a menú
        iniciar3 = (Button) findViewById(R.id.btnCancelar);

        iniciar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar3);
            }
        });


        //clic en icono canasta pedido para ir a pedido
        iniciar4 = (ImageView) findViewById(R.id.btncanasta);

        iniciar4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar4 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar4);
            }
        });
    }
}