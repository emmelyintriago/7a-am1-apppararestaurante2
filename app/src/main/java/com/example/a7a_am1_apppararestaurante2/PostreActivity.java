package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PostreActivity extends AppCompatActivity {

    ImageView iniciar = null;
    ImageView iniciar2 = null;
    ImageView iniciar3 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postre);

        iniciar = (ImageView) findViewById(R.id.btnDescFresa);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), PostresDescrp.class);
                startActivity(cambiar3);
            }
        });

        //Clic en la imágen de canasta para ir al pedido
        iniciar2 = (ImageView) findViewById(R.id.btncanasta);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar2);
            }
        });


        //Clic en la flecha de regresar para ir al menú
        iniciar3 = (ImageView) findViewById(R.id.btnRegresar);

        iniciar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar3);
            }
        });
    }
}