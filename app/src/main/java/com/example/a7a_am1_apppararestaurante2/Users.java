package com.example.a7a_am1_apppararestaurante2;

public class Users {
    public String id;
    public String nombre;
    public String contraseña;
    public String correo;
    public String direccion;
    public String telefono;

    private  Users(){

    }

    public Users(String id, String nombre, String contraseña, String correo, String direccion, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.contraseña = contraseña;
        this.correo = correo;
        this.direccion = direccion;
        this.telefono = telefono;
    }

}
