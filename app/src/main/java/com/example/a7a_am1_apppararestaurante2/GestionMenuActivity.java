package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class GestionMenuActivity extends AppCompatActivity {

    ImageView regresar = null;
    TextView añadirHam = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_menu);

        regresar = (ImageView) findViewById(R.id.btnRegresar);

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar1 = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar1);
            }
        });

        añadirHam = (TextView) findViewById(R.id.btnañadirHam);

        añadirHam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), AddHamburguesaActivity.class);
                startActivity(cambiar2);
            }
        });
    }
}