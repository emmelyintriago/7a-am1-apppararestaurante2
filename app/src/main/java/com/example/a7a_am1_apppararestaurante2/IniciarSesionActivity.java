package com.example.a7a_am1_apppararestaurante2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class IniciarSesionActivity extends AppCompatActivity {


    Button iniciar2 = null;
    Button iniciar = null;

    //Variable global de nuestra instancia
    FirebaseDatabase miBase;
    DatabaseReference miReferencia;

    //Arreglo de los usuarios
    ArrayList<Users> listaUsuarios;

    //Objetos que usaremos para validar el campo correo
    EditText editTextTextEmailAddress2;
    //EditText editTextTextPassword2;
    Button btnIngresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion2);

        //Llamamos a una instancia de la base de datos
        miBase = FirebaseDatabase.getInstance();
        miReferencia = miBase.getReference();

        listaUsuarios = new ArrayList<>();
        //Asociamos la información del dato al objeto
        editTextTextEmailAddress2 = findViewById(R.id.editTextTextEmailAddress2);
        btnIngresar = findViewById(R.id.btnIngresar);

        miReferencia.child("Usuarios").addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChildren()){
                    listaUsuarios.clear();
                    for (DataSnapshot dato: dataSnapshot.getChildren()){
                        //Traemos el usuario de la db y debe tener la estructura de la clase Users
                        Users unUsuario= dato.getValue(Users.class);
                        //Agregamos ese usuario a la lista
                        listaUsuarios.add(unUsuario);
                        //Mostramos unmensaje con el nombre del usuario
                        Toast.makeText(IniciarSesionActivity.this, "Usuario: "+unUsuario.nombre, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Guardamos la información que se digita en el edittext
                String correo = editTextTextEmailAddress2.getText().toString();
                boolean correcto = false;
                //Recorremos la lista de usuarios para verificar que existe
                for (Users miusuario: listaUsuarios){
                    //Preguntamos si el correo y la contraseña son iguales a los del usuario
                    if(correo.equalsIgnoreCase(miusuario.correo)){
                        correcto = true;
                        Toast.makeText(IniciarSesionActivity.this, "Correcto: "+miusuario.nombre, Toast.LENGTH_LONG).show();
                        Intent intencion = new Intent(IniciarSesionActivity.this, MenuActivity.class);
                        startActivity(intencion);

                    }
                    //Si el correo no existe, se mostrará el siguiente mensaje
                    if ( !correcto ){
                        Toast.makeText(IniciarSesionActivity.this, "Usuario no existe", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        //clic en registrarme para ir a crear cuenta
        iniciar2 = (Button) findViewById(R.id.btnRegistrar);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), RegistroActivity.class);
                startActivity(cambiar2);
            }
        });

        iniciar = (Button) findViewById(R.id.btnIngresar);
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar);
            }
        });



    }

}