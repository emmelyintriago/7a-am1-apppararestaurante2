package com.example.a7a_am1_apppararestaurante2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.jetbrains.annotations.NotNull;

public class AdminActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        bottomNavigationView = findViewById(R.id.boton_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(listener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener listener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {


            if(item.getItemId()== R.id.fragmentUno){
                Fragmentos(new FragmentUno());
            }

            if(item.getItemId()== R.id.fragmentDos){
                Fragmentos(new FragmentDos());
            }
            if(item.getItemId()== R.id.fragmentTres){
                Fragmentos(new FragmentTres());
            }
            if(item.getItemId()== R.id.fragmentCuatro){
                Fragmentos(new FragmentCuatro());
            }
            if(item.getItemId()== R.id.fragmentProductos){
                Fragmentos(new ProductosFragment());
            }

            return true;
        }
    };

    private void Fragmentos(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commit();
    }
}