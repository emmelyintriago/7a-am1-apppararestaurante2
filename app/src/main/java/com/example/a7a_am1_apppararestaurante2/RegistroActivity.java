package com.example.a7a_am1_apppararestaurante2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {

    private EditText mEditTextUsuario;
    private EditText mEditTextCorreo;
    private EditText mEditTextPassword;
    private EditText mEditTextDireccion;
    private EditText mEditTextTelefono;

    private Button mButtonRegister;

    //Button iniciar = null;
    Button iniciar2 = null;

    //-----VARIABLE DE LOS DATOS QUE VAMOS A REGISTRAR--------
    private String usuario = "";
    private String correo = "";
    private String contraseña = "";
    private String direccion = "";
    private String telefono = "";

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mEditTextUsuario = (EditText) findViewById(R.id.editTextTextPersonName2);
        mEditTextCorreo = (EditText) findViewById(R.id.editTextTextEmailAddress);
        mEditTextPassword = (EditText) findViewById(R.id.editTextTextPassword);
        mEditTextDireccion = (EditText) findViewById(R.id.editTextTextDireccion);
        mEditTextTelefono = (EditText) findViewById(R.id.editTextTextTelefono);
        mButtonRegister = (Button) findViewById(R.id.btnCrearCuenta);


        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = mEditTextUsuario.getText().toString();
                correo = mEditTextCorreo.getText().toString();
                contraseña = mEditTextPassword.getText().toString();
                direccion = mEditTextDireccion.getText().toString();
                telefono = mEditTextTelefono.getText().toString();

                if (!usuario.isEmpty() && !correo.isEmpty() && !contraseña.isEmpty() ) {

                    if (contraseña.length() >= 6 ) {
                        registerUsuario();
                    } else {
                        Toast.makeText(RegistroActivity.this, "Debe tener al menos 6 caracteres", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(RegistroActivity.this, "Debe completar todos los campos ", Toast.LENGTH_SHORT).show();
                }
            }

        });

        //clic en cancelar para ir a iniciar sesión
        iniciar2 = (Button) findViewById(R.id.btnCancelar);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), IniciarSesionActivity.class);
                startActivity(cambiar2);
            }
        });
    }

    private void registerUsuario() {
        mAuth.createUserWithEmailAndPassword(correo, contraseña).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    Map<String, Object> map = new HashMap<>();
                    map.put("nombre", usuario);
                    map.put("correo", correo);
                    map.put("contraseña", contraseña);
                    map.put("direccion", direccion);
                    map.put("telefono", telefono);

                    String id = mAuth.getCurrentUser().getUid();

            //----------CREAR USUARIO EN LA BASE DE DATOS---------
                    mDatabase.child("Usuarios").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull @NotNull Task<Void> task2) {
                            if (task2.isSuccessful()){
                               startActivity(new Intent(RegistroActivity.this, MenuActivity.class));
                               finish();
                            }
                            else{
                                Toast.makeText(RegistroActivity.this, "No se crearon los datos correctamente", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }else
                {
                    Toast.makeText(RegistroActivity.this, "No se pudo registrar este usuario", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}