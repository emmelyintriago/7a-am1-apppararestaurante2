package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinalActivity extends AppCompatActivity {

    Button iniciar = null;
    Button iniciar2 = null;
    TextView iniciar3 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        iniciar = (Button) findViewById(R.id.btnFactura);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(), FacturaActivity.class);
                startActivity(cambiar);
            }
        });


        iniciar2 = (Button) findViewById(R.id.btnInicio);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar2);
            }
        });


        iniciar3 = (TextView) findViewById(R.id.btnregresar3);

        iniciar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar3);
            }
        });


    }
}