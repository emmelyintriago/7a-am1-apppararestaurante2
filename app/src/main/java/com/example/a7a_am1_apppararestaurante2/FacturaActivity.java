package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class FacturaActivity extends AppCompatActivity {

    Button iniciar = null;
    Button iniciar2 = null;
    ImageView iniciar3 = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_factura);

        iniciar = (Button) findViewById(R.id.btnInicio);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar);
            }
        });

        //clic en cancelar para ir al pedido
        iniciar2 = (Button) findViewById(R.id.btnRegistrar);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar2);
            }
        });

        //clic en retroceder para ir a final
        iniciar3 = (ImageView) findViewById(R.id.btnRegresar2);

        iniciar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar3);
            }
        });
    }
}