package com.example.a7a_am1_apppararestaurante2;

import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.internal.Storage;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class AddHamburguesaActivity extends AppCompatActivity {

    private ImageView imagenpro;
    private EditText nombrepro, descripcionpro, preciopro, stockpro, tamanopro;
    private Button agregarpro;

    private static final int Gallery_Pick = 1;
    private Uri imagenUri;
    private String productoRandonKey, downloadUri;
    private StorageReference ProductoImagenRef;
    private DatabaseReference ProductoRef;
    private ProgressDialog dialog;
    private String Categoria, Nom, Desc, Prec, Stoc, Tama, CurrentDate,CurrentTime;

    public AddHamburguesaActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hamburguesa);



       //Toast.makeText(this, Categoria, Toast.LENGTH_SHORT).show();

        //indicamos un nombre de variable a cada elemento del activity medante su id
        imagenpro = (ImageView)findViewById(R.id.imagenpro);
        nombrepro = (EditText)findViewById(R.id.nombrepro);
        descripcionpro = (EditText)findViewById(R.id.descripcionpro);
        preciopro = (EditText)findViewById(R.id.preciopro);
        stockpro= (EditText)findViewById(R.id.stockpro);
        tamanopro = (EditText)findViewById(R.id.tamanopro);
        agregarpro = (Button)findViewById(R.id.agregarpro);
        dialog = new ProgressDialog( this);

        /*
        imagenpro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbrirGaleria();
            }
        }); */
        agregarpro.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                ValidarProducto();
            }
        });

    }

    //Metodo para abrir la galeria del dispositivo y seleccionar una foto
    private void AbrirGaleria() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/");
        startActivityForResult(intent, Gallery_Pick);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode==Gallery_Pick && resultCode==RESULT_OK && data != null){
            imagenUri= data.getData();
            imagenpro.setImageURI(imagenUri);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    //validamos que todos los campos del producto se encuentren llenos
    private void ValidarProducto() {
        Nom = nombrepro.getText().toString();
        Desc = descripcionpro.getText().toString();
        Prec = preciopro.getText().toString();
        Stoc = stockpro.getText().toString();
        Tama = tamanopro.getText().toString();

        if (TextUtils.isEmpty(Nom)){
            Toast.makeText(this, "Debes ingresar el nombre del producto", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(Desc)){
            Toast.makeText(this, "Debes ingresar la descripcion del producto", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(Prec)){
            Toast.makeText(this, "Debes ingresarel el precio del producto", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(Stoc)){
            Toast.makeText(this, "Debes ingresar el sctock del producto", Toast.LENGTH_SHORT).show();
        }else if (TextUtils.isEmpty(Tama)){
            Toast.makeText(this, "Debes ingresarel tamaño del producto", Toast.LENGTH_SHORT).show();
        }else{
            //llamamos al método para guardar los productos
            GuardarInformacionProducto();
        }

    }

    //Método para guardar
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void GuardarInformacionProducto() {

        dialog.setTitle("Guardado Producto");
        dialog.setMessage("Por favor esper mientras guardamos los productos");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Calendar calendar= Calendar.getInstance();

        SimpleDateFormat curreDataFormat = new SimpleDateFormat("MM-dd-yyyy");
        CurrentDate = curreDataFormat.format(calendar.getTime());

        SimpleDateFormat CurrenTimeFormat = new SimpleDateFormat("HH-mm-ss");
        CurrentTime = CurrenTimeFormat.format(calendar.getTime());

        productoRandonKey = CurrentDate + CurrentTime;
/*
        //almacenamos la imagen previamente seleccionada
        final StorageReference filePath = ProductoImagenRef.child(imagenUri.getLastPathSegment() + productoRandonKey+ ".jpg");
        final UploadTask uploadTask = filePath.putFile(imagenUri);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull @NotNull Exception e) {
                String mensaje = e.toString();
                Toast.makeText(AddHamburguesaActivity.this, "Error"+mensaje, Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(AddHamburguesaActivity.this, "Imagen guardada Exitosamente", Toast.LENGTH_SHORT).show();

                Task<Uri> uriTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull @NotNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()){
                            throw task.getException();
                        }
                        downloadUri = filePath.getDownloadUrl().toString();
                        return  filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<Uri> task) {
                        if (task.isSuccessful()){
                            downloadUri = task.getResult().toString();
                            Toast.makeText(AddHamburguesaActivity.this, "iamgen guardada en Firebase", Toast.LENGTH_SHORT).show();
                            //llamamos al metodo para almacenar los datos
                            GuardarFirebase();
                        }else {
                            Toast.makeText(AddHamburguesaActivity.this, "Error......", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }); */
        GuardarFirebase();
    }

    //almacenar los datos
    private void GuardarFirebase() {
        HashMap<String, Object>  map = new HashMap<>();
        map.put("pid", productoRandonKey);
        map.put("fecha", CurrentDate);
        map.put("hora", CurrentTime);
        map.put("descripcion", Desc);
        map.put("nombre", Nom);
        map.put("precio", Prec);
        map.put("stock", Stoc );
        map.put("tamaño", Tama);


        ProductoRef.child(productoRandonKey).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            //en caso de completarse la tarea con éxito se muestra un mensaje al usuario, si existe un error se mostrará otro mensaje
            @Override
            public void onComplete(@NonNull @NotNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intent = new Intent(AddHamburguesaActivity.this, FragmentUno.class);
                    startActivity(intent);
                    dialog.dismiss();
                    Toast.makeText(AddHamburguesaActivity.this, "Solicitud Exitosa", Toast.LENGTH_SHORT).show();

                } else {
                    dialog.dismiss();
                    String mensaje = task.getException().toString();
                    Toast.makeText(AddHamburguesaActivity.this, "Error:" + mensaje, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}

