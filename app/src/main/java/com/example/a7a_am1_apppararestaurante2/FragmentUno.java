package com.example.a7a_am1_apppararestaurante2;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


public class FragmentUno extends Fragment {

    private View fragmento;
    private ImageView postres, papas, hamburguesas, bebidas;

    public FragmentUno() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmento = inflater.inflate(R.layout.fragment_uno, container, false);

        postres = (ImageView)fragmento.findViewById(R.id.btnpostre);
        papas = (ImageView)fragmento.findViewById(R.id.btnPapas);
        hamburguesas = (ImageView)fragmento.findViewById(R.id.btnhamburguesa);
        bebidas = (ImageView)fragmento.findViewById(R.id.btnbebidas);

        postres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddHamburguesaActivity.class);
                intent.putExtra("categoria", "postres");
                startActivity(intent);

            }
        });

        papas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddHamburguesaActivity.class);
                intent.putExtra("categoria", "papas");
                startActivity(intent);

            }
        });

        hamburguesas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddHamburguesaActivity.class);
                intent.putExtra("categoria", "hamburguesas");
                startActivity(intent);

            }
        });
        bebidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddHamburguesaActivity.class);
                intent.putExtra("categoria", "bebidas");
                startActivity(intent);

            }
        });


        return fragmento;
    }
}