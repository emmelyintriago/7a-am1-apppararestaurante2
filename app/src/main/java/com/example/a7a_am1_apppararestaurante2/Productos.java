package com.example.a7a_am1_apppararestaurante2;



public class Productos {
    private String pid;
    private String nombre;
    private String precio;
    private String descripcion;
    private String stock;
    private String tamano;
    private String categoria;
    private String imagen;
    private String fecha;
    private String hora;



    public Productos(){

    }

    public Productos(String pid, String nombre, String precio, String descripcion, String stock, String tamano, String categoria) {
        this.pid = pid;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.stock = stock;
        this.tamano = tamano;
        this.categoria = categoria;
        this.imagen = imagen;
        this.fecha = fecha;
        this.hora = hora;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String uid) {
        pid = pid;
    }

    public String getNombre() {return nombre;}

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {this.hora= hora;}

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
