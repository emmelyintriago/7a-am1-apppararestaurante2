package com.example.a7a_am1_apppararestaurante2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class BebidasActivity2 extends AppCompatActivity {

    ImageView iniciar = null;
    ImageView iniciar2 = null;
    ImageView iniciar3 = null;

    //Clic en la imágen bebida para ir a la descripción del producto
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas2);


        //Clic en la imágen de canasta para ir al pedido
        iniciar2 = (ImageView) findViewById(R.id.btncanasta);

        iniciar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar2 = new Intent(getApplicationContext(), PedidoActivity.class);
                startActivity(cambiar2);
            }
        });


        //Clic en la flecha de regresar para ir al menú
        iniciar3 = (ImageView) findViewById(R.id.btnRegresar);

        iniciar3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambiar3 = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(cambiar3);
            }
        });
    }
}