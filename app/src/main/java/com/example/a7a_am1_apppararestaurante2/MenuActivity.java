package com.example.a7a_am1_apppararestaurante2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.a7a_am1_apppararestaurante2.Productos;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private FirebaseAuth auth;
    private String CurrentUserId;
    private DatabaseReference UserRef, ProductosRef;
    private String Correo;
    private FloatingActionButton botonFlotante;
    private RecyclerView reciclerMenu;
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


/*
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            Correo = bundle.getString("correo");
        }
*/
        auth = FirebaseAuth.getInstance();
        //CurrentUserId = auth.getCurrentUser().getUid();
        UserRef = FirebaseDatabase.getInstance().getReference().child("Usuarios");

        ProductosRef = FirebaseDatabase.getInstance().getReference().child("Productos");
        reciclerMenu = findViewById(R.id.recicler_menu);
        reciclerMenu.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        reciclerMenu.setLayoutManager(layoutManager) ;
        botonFlotante = (FloatingActionButton)findViewById(R.id.fab);

        botonFlotante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, AddHamburguesaActivity.class);
                startActivity(intent);
            }
        });
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("PlanetFood");
        setActionBar(toolbar);



        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        TextView nombreHeader = (TextView) headerView.findViewById(R.id.usuario_nombre_perfil);
        CircleImageView imagenHeader = (CircleImageView) headerView.findViewById(R.id.usuario_imagen_perfil);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }else {
            super.onBackPressed();
        }
    }

    //----Creacion del menu en la navegacion
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_pincipal_drawer,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    public boolean onNavigationItemSelected(@NonNull MenuItem item){
        int id = item.getItemId();

        if (id ==R.id.nav_carrito){
            ActivityCarrito();
        }
        else if (id == R.id.nav_categorias) {
            ActivityCategoria();
        }
        else if (id == R.id.nav_perfil) {
            ActivityPerfil();
        }
        else if (id == R.id.nav_falla) {
            ActivityFalla();
        }
        else if (id == R.id.nav_salir) {
            auth.signOut();
            EnviarAlogin();
        }

        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }



    //---- ojo ----eliminar el perfil para no mostrarlo en la app
    //en Registroactivity deberia ir el perfil
    private void ActivityPerfil() {
        Intent intent = new Intent(MenuActivity.this, RegistroActivity.class);

    }

    private void ActivityCategoria() {
        Intent intent = new Intent(MenuActivity.this, AdminActivity.class);
        startActivity(intent);
    }

    //Falla
    private void ActivityFalla() {
        Intent intent = new Intent(MenuActivity.this, FallaActivity.class);
        startActivity(intent);
    }

    private void ActivityCarrito() {
    }


    private void EnviarSetup() {
        Intent intent = new Intent(MenuActivity.this, IniciarSesionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("correo", Correo);
        startActivity(intent);
        finish();
    }

    private void EnviarAlogin() {
        Intent intent = new Intent(MenuActivity.this, RegistroActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}